## Scores Pro
Run:

- `composer install`

Run migrations:

- `php artisan migrate`

Run Seeder:

- `php artisan db:seed`

Then run

- `php artisan command:clientStructure`
- `php artisan command:clientUpdate`

Move both `clientStructure.conf` and `clientUpdate.conf` service to `/etc/init`