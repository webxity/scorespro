<?php

class HomeController extends BaseController {

        //8016157
    public static function GetData()
    {
        echo "<pre>";
        //$Hid = 8132158;
        $Hid = MatchModel::getMatchHid();
        $url = 'http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=1&s=1&h='.$Hid;
        $sports_request      = new GuzzleHttp\Client();
        $sports_response     = $sports_request->get($url);
        $getStatusCode       = $sports_response->getStatusCode();
        if ( $getStatusCode == "200" ) {
            $sports_data   = [];
            $sports_data_info   = [];
            $data       =  $sports_response->getBody()->getContents();
            $sports_xml = simplexml_load_string($data);
            $match_hid_data = (array)$sports_xml->Sport;
            $match_hid = $match_hid_data['@attributes']['hid'];
            //print_r($match_hid);exit();
            $sport = isset($sports_xml->Sport->Matchday->Match)? $sports_xml->Sport->Matchday->Match : null;
            $err = $sports_xml->GeneralInformation->Error;

            if (!$sport && $err) return Log::critical($err); MatchModel::updateMatchHidByHid($Hid,$match_hid);

            if (isset($sport)) {
                $Match_day  = (array) $sports_xml->Sport->Matchday;
                $match_date = $Match_day['@attributes']['date'];

                foreach( $sport as $result )
                {
                    foreach( (array) $result as $index => $val)
                    {
                            $sports_data[$index] = $val;
                    }
                    $sports_data_info[] = $sports_data;
                }
                $country_array =[];
                //print_r($sports_data_info);
                foreach ($sports_data_info as $value) {
                    # code...
                    //print_r($value);
                    $match_data = [];
                    $match_info = static::toarray($value['Information']);
                    $season_id = seasonsModel::getSeasonById($match_info['season']);
                    $country_code = countriesModel::getCountryId($match_info['country'],$season_id );
                    
                    $venues_info = static::insertVenues($country_code[0]->country_code);
                    $team_info = static::insertTeam($country_code[0]->country_id,$value['@attributes']['leagueCode']);                       
                    if(isset($venues_info) && count($venues_info)){
                        VenuesModel::insertVenuesInfo($venues_info);                        
                    } 
                    if(isset($team_info) && count($team_info)){
                        TeamsModel::insertTeamsInfo($team_info, $country_code[0]->country_id);                        
                    }
                    $teams_array = TeamsModel::getAllTeams();
                    $home_team = static::toarray($value['Home']); 
                    $away_team = static::toarray($value['Away']);
                    //$results   = static::toarray($value['Results']);
                    $parentcompetition_id = isset($match_info['parentCompetition']) ? CompetitionsModel::getCompetitionByName($match_info['parentCompetition'],$country_code[0]->country_id) :  '';
                    $value['@attributes']['home_team_id'] = $home_team['@attributes']['id'];
                    $value['@attributes']['away_team_id'] = $away_team['@attributes']['id'];
                    $value['@attributes']['hid'] = $match_hid;

                    $value['@attributes']['match_date'] = $match_date;
                    $match_info['match_id'] = $value['@attributes']['id'];
                    $match_info['season_id'] = $season_id;
                    $match_info['country_id'] = $country_code[0]->country_id;
                    $match_info['match_parent_competition_id'] = $parentcompetition_id; 
                    $match_info['match_root_competition_id'] = $value['@attributes']['leagueCode'];
                    $match_info['hid'] = $match_hid;
                    if(in_array($home_team['@attributes']['id'], $teams_array) && in_array($away_team['@attributes']['id'], $teams_array)){
                        MatchModel::insertMatch($value['@attributes']);
                        $match_results = (array) $value['Results'];
                        if(isset($away_team['Cards'])){
                            $away_team_card = (array) $away_team['Cards'];
                            MatchCardsModel::insertMatchCard($away_team_card, $value['@attributes']['id'], $away_team['@attributes']['id']);
                        }
                        if(isset($home_team['Cards'])){
                            $home_team_card = (array) $home_team['Cards'];
                            MatchCardsModel::insertMatchCard($home_team_card, $value['@attributes']['id'], $home_team['@attributes']['id']);
                        }
                        
                        foreach ($match_results['Period'] as $match_peroid) {
                            $match_peroid_data = (array) $match_peroid;
                            MatchPeriodModel::insertPeroidResults($match_peroid_data,$value['@attributes']['id']);
                        }
                        foreach ($match_results['Result'] as $match_result) {
                            MatchResultsModel::insertMatchResults((array) $match_result, $value['@attributes']['id']);
                            # code...
                        }    
                        MatchEventsModel::setData($value);
                        MatchInfoModel::insertMatchInfo($match_info);
                
                    }
                    sleep(10);
                }
            }
        }
    }

    public static function insertVenues($country_code = ''){
        //Venues
        $venues_request      = new GuzzleHttp\Client();
        $venues_response     = $venues_request->get('http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=28&s=1&code='.$country_code);
        $getStatusCode       = $venues_response->getStatusCode();

        if ( $getStatusCode == "200" ) {
            $venues_data   = [];
            $data          =  $venues_response->getBody()->getContents();
            $venues_xml    = simplexml_load_string($data);
            $venue_info    = (array) $venues_xml->List;
            $venue         = isset($venue_info['Venue']) ? $venue_info['Venue'] : [];
            foreach( $venue as $result )
            {

                foreach( (array) $result as $index => $val)
                {
                        $venues_data[] = $val;
                }

            }

            if ( !empty( $venues_data  ) && count($venues_data) > 0 )
            {
                return $venues_data;
            }
        }
    }

    public static function insertTeam($country_code = '', $league_id = 0){
        //Venues
        $venues_request      = new GuzzleHttp\Client();
        $venues_response     = $venues_request->get('http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=17&s=1&c='.$country_code.'&id='.$league_id);
        $getStatusCode       = $venues_response->getStatusCode();

        if ( $getStatusCode == "200" ) {
            $venues_data   = [];
            $data          =  $venues_response->getBody()->getContents();
            $venues_xml    = simplexml_load_string($data);
            $venue_info    = $venues_xml->List->Item;
            $venue         = isset($venue_info) ? $venue_info : [];
            foreach( $venue as $result )
            {

                foreach( (array) $result as $index => $val)
                {
                        $venues_data[] = $val;
                }

            }

            if ( !empty( $venues_data  ) && count($venues_data) > 0 )
            {
                return $venues_data;
            }
        }
    }

    public static function toarray($data_info)
    {
        $data = [];
        foreach( (array) $data_info as $index => $val)
        {
            $data[$index] = $val;
        }
        return $data;
    }
}
