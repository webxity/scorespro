<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ScoresproClientStructure extends Command {

	/**
	 * The console command name.
	 * @var string
	 */
	protected $name = 'command:clientStructure';

	/**
	 * The console command description.
	 * @var string
	 */
	protected $description = 'client structure';

    /**
     * Create a new command instance.
     */
	public function __construct()
	{
		parent::__construct();

	}

	/**
	 * Execute the console command.
	 * @return mixed
	 */
	public function fire()
	{
        echo "Structure call\n";
        echo "--------------\n";

        ClientStructure::getData();

        echo("Poller Ended\n\n");
	}

	/**
	 * Get the console command arguments.
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', null, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
