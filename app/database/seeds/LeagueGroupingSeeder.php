<?php

class LeagueGroupingSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('league_grouping')->delete();

        $collections = [
            0 => [
                'group_id' => 'NO GROUP',
                'group_items' => 3,
                'group_name' => -1
            ],
            1 => [
                'group_id' => 'HOME',
                'group_items' => 5,
                'group_name' => 1
            ],
            2 => [
                'group_id' => 'AWAY',
                'group_items' => 5,
                'group_name' => 2
            ],
            3 => [
                'group_id' => 'TOTAL',
                'group_items' => 5,
                'group_name' => 3
            ],
            4 => [
                'group_id' => 'NO GROUP',
                'group_items' => 4,
                'group_name' => -1
            ]
        ];

        foreach($collections as $collection) {
            LeagueGroupingModel::create($collection);
        }
	}

}
