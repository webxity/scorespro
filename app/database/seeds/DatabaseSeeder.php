<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('LeagueGroupingSeeder');
		$this->call('LeagueGroupingLabelSeeder');

        $this->command->info('Tables seeded!');
	}

}
