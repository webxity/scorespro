<?php

class LeagueGroupingLabelSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('league_grouping_label')->delete();

        $collections = [
            [
                'group' => -1,
                'group_label_id' => 'p',
                'group_label_name' => '.P'
            ],
            [
                'group' => -1,
                'group_label_id' => 'tn',
                'group_label_name' => 'TEAM'
            ],
            [
                'group' => -1,
                'group_label_id' => '1',
                'group_label_name' => 'PL'
            ],
            [
                'group' => 1,
                'group_label_id' => '2',
                'group_label_name' => 'W'
            ],
            [
                'group' => 1,
                'group_label_id' => '3',
                'group_label_name' => 'D'
            ],
            [
                'group' => 1,
                'group_label_id' => '4',
                'group_label_name' => 'L'
            ],
            [
                'group' => 1,
                'group_label_id' => '5',
                'group_label_name' => 'F'
            ],
            [
                'group' => 1,
                'group_label_id' => '6',
                'group_label_name' => 'A'
            ],
            [
                'group' => 2,
                'group_label_id' => '7',
                'group_label_name' => 'W'
            ],
            [
                'group' => 2,
                'group_label_id' => '8',
                'group_label_name' => 'D'
            ],
            [
                'group' => 2,
                'group_label_id' => '9',
                'group_label_name' => 'L'
            ],
            [
                'group' => 2,
                'group_label_id' => '10',
                'group_label_name' => 'F'
            ],
            [
                'group' => 2,
                'group_label_id' => '11',
                'group_label_name' => 'A'
            ],
            [
                'group' => 3,
                'group_label_id' => '12',
                'group_label_name' => 'W'
            ],
            [
                'group' => 3,
                'group_label_id' => '13',
                'group_label_name' => 'D'
            ],
            [
                'group' => 3,
                'group_label_id' => '14',
                'group_label_name' => 'L'
            ],
            [
                'group' => 3,
                'group_label_id' => '15',
                'group_label_name' => 'F'
            ],
            [
                'group' => 3,
                'group_label_id' => '16',
                'group_label_name' => 'A'
            ],
            [
                'group' => -1,
                'group_label_id' => '17',
                'group_label_name' => 'GD'
            ],
            [
                'group' => -1,
                'group_label_id' => '18',
                'group_label_name' => '+/-'
            ],
            [
                'group' => -1,
                'group_label_id' => '19',
                'group_label_name' => 'Pts'
            ],
            [
                'group' => -1,
                'group_label_id' => 'st',
                'group_label_name' => ''
            ]
        ];

        foreach($collections as $collection) {
            LeagueGroupingLabelModel::create($collection);
        }
	}

}
