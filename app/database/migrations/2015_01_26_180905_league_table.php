<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeagueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('league_table', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('competition_id')->unsigned();
			$table->integer('team_id')->unsigned();
            $table->string('group_id');

            // Position/Ranking
			$table->smallInteger('position');

            // Team Name
            $table->string('team_name');

            // Played Matches
            $table->integer('matches_played');

            // HOME W WINS ins
            $table->integer('home_wins');

            // HOME DRAWS
            $table->integer('home_draws');

            // HOME LOSES
            $table->integer('home_loses');

            // HOME Goals For
            $table->integer('home_goals_for');

            // HOME Goals Against
            $table->integer('home_goals_against');

            // AWAY WINS
            $table->integer('away_wins');

            // AWAY DRAWS
            $table->integer('away_draws');

            // AWAY LOSES
            $table->integer('away_loses');

            // AWAY Goals For
            $table->integer('away_goals_for');

            // AWAY Goals Against
            $table->integer('away_goals_agains');

            // TOTAL WINS
            $table->integer('wins');

            // TOTAL DRAWS
            $table->integer('draws');

            // TOTAL LOSES
            $table->integer('loses');

            // TOTAL Goals For
            $table->integer('goals_for');

            // TOTAL Goals Against
            $table->integer('goals_against');

            // Goals difference (i.e. +12)
            $table->integer('goal_diff');

            // Deducted Points (i.e. -3)
            $table->integer('deducted_points');

            // Points
            $table->integer('points');

            // Promotion or Relegation info if applicable
            // (i.e. “Ch. League”, which means that this position promotes to Champions League)
            $table->string('st');

			$table->foreign('competition_id')->references('competition_id')->on('competitions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('league_table');
	}

}
