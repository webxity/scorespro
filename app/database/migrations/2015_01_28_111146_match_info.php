<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_info', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->integer('season_id')->unsigned();
			$table->integer('country_id')->unsigned();
			$table->integer('parent_competition_id')->unsigned()->nullable();
			$table->integer('root_competition_id')->unsigned()->nullable();
			$table->string('short_name', 32);
			$table->string('round', 32);
			$table->text('note');
			$table->string('bit_array', 20);
			$table->string('info_timestamp', 32);

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_info');
	}

}
