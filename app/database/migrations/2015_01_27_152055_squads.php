<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Squads extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('squads', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('competition_id')->unsigned();
			$table->integer('team_id')->unsigned();
			$table->integer('player_id')->unsigned();

			$table->foreign('competition_id')->references('competition_id')->on('competitions');
			$table->foreign('team_id')->references('team_id')->on('teams');
			$table->foreign('player_id')->references('player_id')->on('players');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('squads');
	}

}
