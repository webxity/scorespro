<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchLineups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_lineups', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('player_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('match_id')->unsigned();
            $table->string('name');
            $table->string('nationality');
            $table->integer('shirt');
            $table->string('position');
            $table->integer('active');

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_lineups');
	}

}
