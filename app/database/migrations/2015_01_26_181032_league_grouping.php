<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeagueGrouping extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('league_grouping', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('group_id', 32);
			$table->tinyInteger('group_items');
			$table->tinyInteger('group_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('league_grouping');
	}

}
