<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Competitions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competitions', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('competition_id')->unsigned()->unique();
			$table->integer('competition_parent_id')->nullable()->unsigned();
			$table->integer('country_id')->unsigned();
			$table->string('competition_code');
			$table->string('competition_name');
			$table->text('competition_description');
			$table->integer('competition_sort');
			$table->date('competition_start');
			$table->date('competition_end');

			$table->foreign('country_id')->references('country_id')->on('countries');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competitions');
	}

}
