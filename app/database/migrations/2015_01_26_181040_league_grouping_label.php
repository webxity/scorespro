<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LeagueGroupingLabel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('league_grouping_label', function(Blueprint $table)
		{
			$table->increments('id');
			$table->tinyInteger('group');
			$table->string('group_label_id', 32);
			$table->string('group_label_name', 32);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('league_grouping_label');
	}

}
