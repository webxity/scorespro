<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchScorer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_scorer', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->integer('team_id')->unsigned();

            // Player Name
			$table->string('player_name', 32);

            // Period id which indicate, 1st half, 2nd half, extra time, penalty
			$table->smallInteger('period_id');

            // Match time scorer scored
			$table->smallInteger('match_time');

            // Score Type? Penalty, Own Goal, given card and score
			$table->string('score_type');

            // Player ID (Not sure about this, always returns 0 from API)
			$table->integer('p_id');

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_scorer');
	}

}
