<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Venues extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venues', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('venue_id')->unsigned()->unique();
			$table->string('venue_name');
			$table->string('venue_city');
			$table->string('venue_address');
			$table->string('venue_country');
			$table->string('venue_zip');
			$table->integer('venue_capacity');
			$table->string('venue_established');
			$table->string('venue_url');
			$table->decimal('venue_longitude', 9, 6);
			$table->decimal('venue_latitude', 9, 6);
			$table->string('venue_map_url');
			$table->string('venue_surface');
			$table->string('venue_facts');
			$table->string('venue_description');
			$table->string('venue_architect');
			$table->string('venue_prev_name');
			$table->string('venue_phone');
			$table->string('venue_fax');
			$table->string('venue_email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venues');
	}

}
