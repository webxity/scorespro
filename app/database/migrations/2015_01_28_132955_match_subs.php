<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchSubs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_substitution', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->integer('team_id')->unsigned();

            // Player ID for New Player
			$table->integer('player_in_id')->unsigned();

            // Player ID for Replaced Player
            $table->integer('player_out_id')->unsigned();

            // New Player Full Name
            $table->string('player_in_name');

            // Replaced Player Full Name
            $table->string('player_out_name');

            // Time of Substitution (Minute)
            $table->smallInteger('sub_minute');

            // Extra Time of Substitution (applicable in case of 45’+ and 90’+)
            $table->smallInteger('sub_minute2');

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_substitution');
	}

}
