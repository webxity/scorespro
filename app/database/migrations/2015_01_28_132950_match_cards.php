<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchCards extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_cards', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->integer('team_id')->unsigned();

            // Player Name
			$table->string('player_name', 32);

            // Match time the card was given at
			$table->smallInteger('match_time');

            // Card type Red/Yellow
			$table->string('card_type');

            // Player ID
			$table->integer('p_id');

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_cards');
	}

}
