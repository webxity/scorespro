<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Match extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned()->unique();
			$table->integer('competition_id')->unsigned();
			$table->string('ct', 32);
			$table->date('match_date');
			$table->string('last_period', 32);
			$table->string('league_type', 32);
			$table->string('start_time', 32);
			$table->string('match_status', 32);
			$table->string('status_type', 32);
			$table->smallInteger('match_type');
			$table->smallInteger('visible');
			$table->smallInteger('lineups');
			$table->string('venue');
			$table->string('referee');
			$table->integer('hid')->unsigned();
			$table->integer('home_team_id')->unsigned();
			$table->integer('away_team_id')->unsigned();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match');
	}

}
