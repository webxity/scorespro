<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchEvents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_events', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned();

            // Event type name from MatchEeventsModels::$_events
			$table->string('event_type', 32);

            // In case a value thats custom just place the value here
			$table->string('event_value', 32)->nullable();
            $table->timestamps();

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_events');
	}

}
