<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchPeriod extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_period', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->integer('period_id');

            // Period name, 1st half, 2nd half, Extra Time, Penalty
			$table->string('period_name', 32);

            // Period detail name, Score
			$table->string('detail_name', 32);

            // Period detail value, If Score (1-3)
			$table->string('detail_value', 32);

            $table->foreign('match_id')->references('match_id')->on('match');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_period');
	}

}
