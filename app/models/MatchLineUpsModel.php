<?php
class MatchLineUpsModel extends \Eloquent
{
    protected $table = 'match_lineups';

    protected $fillable = [
        'player_id',
        'team_id',
        'match_id',
        'name',
        'active',
        'shirt',
        'position',
        'nationality'
    ];

    public static function insert($lineup_data, $team_lineup)
    {
        $lineups = $team_lineup['Lineup'];

        foreach($lineups as $lineup) {
            if (!isset($lineup['@attributes'])) {
                continue;
            }

            $_lineup = $lineup['@attributes'];

            $data = $ids = [
                'player_id' => $_lineup['id'],
                'match_id' => $lineup_data['match_id'],
                'team_id' => $lineup_data['team_id']
            ];

            $data = array_merge($_lineup, $data);
            unset($data['id']);

            if (self::getMatchLineupsByPMTId($ids)) {
                DB::table('match_lineups')->update($data);
                continue;
            }

            DB::table('match_lineups')->insert($data);
        }
    }

    public static function getMatchLineupsByPMTId($ids)
    {
        return DB::table('match_lineups')
            ->where($ids)
            ->first();
    }
}
