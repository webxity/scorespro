<?php


class CompetitionsModel extends \Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competitions';

    protected $fillable = array('competition_id', 'competition_parent_id', 'country_id', 'competition_code','competition_name','competition_description','competition_sort','competition_start','competition_end');


    public static function competitions($data)
    {
        $competition_array =  static::getAllCompetitionsId();
        if(isset($data['id']) && !in_array($data['id'], $competition_array)){
            DB::table('competitions')->insert(array(
                'competition_id'            => $data['id'],
                'country_id'                => $data['country_id'],
                'competition_parent_id'     => $data['competition_parent_id'],
                'competition_code'          => $data['code'],
                'competition_name'          => $data['name'],
                'competition_description'   => $data['description'],
                'competition_sort'          => $data['sort'],
                'competition_start'         => $data['comp_start'],
                'competition_end'           => $data['comp_end'],
            ));
        }
    }

    public static function getCompetitionByName($competition_name = '', $country_id = 0)
    {
        return DB::table('competitions')->where('competition_name', $competition_name)
        ->where('country_id', $country_id)->pluck('competition_parent_id');
    }

    public static function getAllCompetitions()
    {
        $competition_data = [];
        foreach (CompetitionsModel::all() as $competition)
        {
            $competition_data[$competition->id]['country_id'] = $competition->country_id;
            $competition_data[$competition->id]['competition_id'] = $competition->competition_id;
        }
        return $competition_data;
    }

    public static function getCompetitionById($id = 0) {
            return self::where('competition_id', '=', $id)->first();
    }
    
    public static function getAllCompetitionsId()
    { 
        $competition_data = [];
        
        foreach (CompetitionsModel::all() as $competition)
        {
            $competition_data[] = $competition->competition_id;
        }
        return $competition_data;
    }

    public static function getAllCompetitionsData()
    { 
        $competition_data = [];
        $count = 0;
        foreach (CompetitionsModel::all() as $competition)
        {
            $competition_data[$count]['competition_id'] = $competition->competition_id;
            $competition_data[$count]['country_id'] = $competition->country_id;
            $count++;
        }
        return $competition_data;
    }    
}