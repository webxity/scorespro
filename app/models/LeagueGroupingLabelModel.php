<?php
class LeagueGroupingLabelModel extends \Eloquent
{
    protected $table = 'league_grouping_label';

    public $timestamps = false;

}