<?php
class LeagueTableModel extends \Eloquent
{
    protected $table = 'league_table';

    public static $map_label_group = [
        'p' => 'position',
        'tn' => 'team_name',
        '1' => 'matches_played',
        '2' => 'home_wins',
        '3' => 'home_draws',
        '4' => 'home_loses',
        '5' => 'home_goals_for',
        '6' => 'home_goals_against',
        '7' => 'away_wins',
        '8' => 'away_draws',
        '9' => 'away_loses',
        '10' => 'away_goals_for',
        '11' => 'away_goals_agains',
        '12' => 'wins',
        '13' => 'draws',
        '14' => 'loses',
        '15' => 'goals_for',
        '16' => 'goals_against',
        '17' => 'goal_diff',
        '18' => 'deducted_points',
        '19' => 'points',
        'st' => 'st'
    ];

    public static function GetData()
    {
        $competitionIds = CompetitionsModel::getAllCompetitionsId();

        $request = new GuzzleHttp\Client();

        foreach ( $competitionIds as $competitionID ) {
            self::request($request, $competitionID);
        }
    }

    public static function request($http, $compID)
    {
        if (!($http instanceof GuzzleHttp\Client)) {
            $http = new GuzzleHttp\Client();
        }

        $url = "http://data2.scorespro.com/exporter/?state=clientUpdate";
        $url .= "&usr=14weklapsa&pwd=GiJ89tTs&type=7&s=1&l={$compID}";

        $competition_response = $http->get($url);
        $get_competition_data = $competition_response->getStatusCode();
        if ( $get_competition_data == "200") {
            $get_competition_data = $competition_response->getBody()->getContents();
            $leaguetable_data_xml = simplexml_load_string($get_competition_data);
            $tables = ClientUpdate::formatToArray($leaguetable_data_xml->Sport->Table);

            if (isset($tables['Team']) && !empty($tables['Team'])) {
                $teams = $tables['Team'];

                if (isset($teams['@attributes'])) {
                    $teams = [$teams];
                }

                foreach ($teams as $team) {
                    self::insert($compID, $team);
                }
            }
        }
    }

    /**
     * @param int $compID
     * @param int $team
     * @return mixed
     */
    public static function insert($compID, $team)
    {
        if (!isset($team['@attributes']) || !isset($team['columns'])) {
            return null;
        }

        $team_attrs = $team['@attributes'];
        $columns = $team['columns'];

        $against = [
            'competition_id' => $compID,
            'team_id' => $team_attrs['id']
        ];

        $data = [];

        foreach ($columns['column'] as $column) {
            $attr = $column['@attributes'];
            $labelID = $attr['labelId'];
            $value = $attr['value'];

            $key = self::$map_label_group[$labelID];
            $data[$key] = $value;
        }

        if (isset($team_attrs['groupId'])) {
            $data['group_id'] = $team_attrs['groupId'];
        }

        $data = array_merge($against, $data);

        if (self::getLeagueTable($against)) {
            if (self::getLeagueTable($data)) {
                return null;
            }

            return DB::table('league_table')
                ->where($against)
                ->update($data);
        }

        return DB::table('league_table')->insert($data);
    }

    /**
     * @param array $against Team and Competition
     * @return mixed
     */
    public static function getLeagueTable($against)
    {
        return DB::table('league_table')
            ->where($against)
            ->first();
    }
}
