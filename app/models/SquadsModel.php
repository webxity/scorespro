<?php


class SquadsModel extends \Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'squads';

    protected $fillable = [
        'competition_id',
        'team_id',
        'player_id'
    ];

    /**
     * @param array $teams
     * @param Integer $compID
     * @return mixed
     */
    public static function insertCollection($teams, $compID)
    {
        foreach ( $teams as $team ) {
            self::insert($team, $compID);
        }
    }

    /**
     * @param $team
     * @param $compID
     * @return mixed
     */
    public static function insert($team, $compID)
    {
        $team_id = $team['@attributes']['id'];
        $is_team = TeamsModel::getTeamById($team_id);

        if (!$is_team) {
            $msg = "Team({$team_id}) not found.";
            return Log::warning($msg);
        }

        if (isset($team['Player'])) {
            $players = $team['Player'];

            $callbackFunction = function($player_id) use($compID, $team_id) {
                $data = [
                    'competition_id' => $compID,
                    'team_id' => $team_id,
                    'player_id' => $player_id
                ];

                if (!self::getSquadsByData($data)) {
                    DB::table('squads')->insert($data);
                }
            };

            if (isset($players['@attributes'])) {
                return PlayersModel::insert($players, $callbackFunction);
            }

            foreach ($players as $player) {
                PlayersModel::insert($player, $callbackFunction);
            }
        }
    }

    /**
     * @param array $where
     * @return mixed
     */
    public static function getSquadsByData($where)
    {
        return self::where($where)->first();
    }
}