<?php
class ClientUpdate
{
    public static function GetData()
    {
        //$Hid = 8251861;
        $Hid = MatchModel::getMatchHid();
        $url = 'http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=1&s=1&h='.$Hid;
        $sports_request      = new GuzzleHttp\Client();
        $sports_response     = $sports_request->get($url);
        $getStatusCode       = $sports_response->getStatusCode();
        if ( $getStatusCode == "200" ) {
            $sports_data   = [];
            $sports_data_info   = [];
            $data =  $sports_response->getBody()->getContents();
            $sports_xml = simplexml_load_string($data);
            $match_hid_data = (array)$sports_xml->Sport;

            $err = $sports_xml->GeneralInformation->Error;

            if (empty($match_hid_data) && $err) {
                return Log::critical($err);
            }

            $match_hid = $match_hid_data['@attributes']['hid'];
            $sport = isset($sports_xml->Sport->Matchday->Match)
                ? $sports_xml->Sport->Matchday->Match
                : null;

            if (!$sport) {
                return Log::critical("No match found");
            }

            if (isset($sport)) {
                $Match_day  = (array) $sports_xml->Sport->Matchday;
                $match_date = $Match_day['@attributes']['date'];

                foreach( $sport as $result ) {
                    foreach( (array) $result as $index => $val) {
                            $sports_data[$index] = $val;
                    }
                    $sports_data_info[] = $sports_data;
                }

                foreach ($sports_data_info as $value) {
                    $match_info = static::toarray($value['Information']);
                    $season_id = seasonsModel::getSeasonById($match_info['season']);

                    if (!$season_id) {
                        $msg = "Season({$season_id}) not found.";
                        Log::critical($msg);
                        continue;
                    }

                    $country_code = countriesModel::getCountryId($match_info['country'], $season_id);

                    if (!$country_code) {
                        $msg = "Season({$season_id}) not found for Country({$match_info['country']})";
                        Log::critical($msg);
                        continue;
                    }

                    $venues_info = static::insertVenues($country_code[0]->country_code);
                    $team_info = static::insertTeam(
                        $country_code[0]->country_id,
                        $value['@attributes']['leagueCode']
                    );

                    if (isset($venues_info) && count($venues_info)) {
                        VenuesModel::insertVenuesInfo($venues_info);
                    }

                    if (isset($team_info) && count($team_info)) {
                        TeamsModel::insertTeamsInfo($team_info, $country_code[0]->country_id);                        
                    }

                    $home_team = self::formatToArray($value['Home']);
                    $away_team = self::formatToArray($value['Away']);

                    $parentcompetition_id = isset($match_info['parentCompetition'])
                        ? CompetitionsModel::getCompetitionByName(
                            $match_info['parentCompetition'],
                            $country_code[0]->country_id
                        )
                        :  '';

                    $value['@attributes']['home_team_id'] = $home_team['@attributes']['id'];
                    $value['@attributes']['away_team_id'] = $away_team['@attributes']['id'];
                    $value['@attributes']['hid'] = $match_hid;

                    $value['@attributes']['match_date'] = $match_date;
                    $match_info['match_id'] = $matchID = $value['@attributes']['id'];
                    $match_info['season_id'] = $season_id;
                    $match_info['country_id'] = $country_code[0]->country_id;
                    $match_info['parent_competition_id'] = $parentcompetition_id;
                    $match_info['root_competition_id'] = $value['@attributes']['leagueCode'];
                    $match_info['hid'] = $match_hid;

                    $home_team_id = $home_team['@attributes']['id'];
                    $home_team_id_exist = TeamsModel::getTeamById($home_team_id);

                    $away_team_id = $away_team['@attributes']['id'];
                    $away_team_id_exist = TeamsModel::getTeamById($away_team_id);

                    MatchModel::$matchID = $matchID;

                    if (!$home_team_id_exist) {
                        $msg = "Team({$home_team_id}) not " .
                            "found for Match({$matchID})";
                        Log::critical($msg);
                        continue;
                    }

                    if (!$away_team_id_exist) {
                        $msg = "Team({$away_team_id}) not " .
                            "found for Match({$matchID})";
                        Log::critical($msg);
                        continue;
                    }

                    MatchModel::insertMatch($value['@attributes']);

                    MatchModel::$homeTeamID = $home_team_id;
                    MatchModel::$awayTeamID = $away_team_id;

                    if (isset($home_team['Cards'])) {
                        MatchModel::$teamID = $home_team_id;
                        MatchCardsModel::insertMatchCard($home_team['Cards']);
                    }

                    if (isset($away_team['Cards'])) {
                        MatchModel::$teamID = $away_team_id;
                        MatchCardsModel::insertMatchCard($away_team['Cards']);
                    }

                    $match_results = isset($value['Results'])
                        ? self::formatToArray($value['Results'])
                        : null;

                    if ($match_results) {
                        foreach ($match_results['Period'] as $match_peroid) {
                            MatchPeriodModel::insertPeroidResults($match_peroid);
                        }

                        foreach ($match_results['Result'] as $match_result) {
                            MatchResultsModel::insertMatchResults($match_result);
                        }

                        if (isset($match_results['Scorers'])) {
                            MatchScorerModel::insertScorer($match_results['Scorers']);
                        }
                    }

                    MatchEventsModel::setData(self::formatToArray($value));
                    MatchInfoModel::insertMatchInfo($match_info);

                    sleep(10);
                }
            }
        }
    }

    public static function insertVenues($country_code = ''){
        //Venues
        $venues_request      = new GuzzleHttp\Client();
        $venues_response     = $venues_request->get('http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=28&s=1&code='.$country_code);
        $getStatusCode       = $venues_response->getStatusCode();

        if ( $getStatusCode == "200" ) {
            $venues_data   = [];
            $data          =  $venues_response->getBody()->getContents();
            $venues_xml    = simplexml_load_string($data);
            $venue_info    = (array) $venues_xml->List;
            $venue         = isset($venue_info['Venue']) ? $venue_info['Venue'] : [];
            foreach( $venue as $result )
            {

                foreach( (array) $result as $index => $val)
                {
                        $venues_data[] = $val;
                }

            }

            if ( !empty( $venues_data  ) && count($venues_data) > 0 )
            {
                return $venues_data;
            }
        }
    }

    public static function insertTeam($country_code = '', $league_id = 0) {
        //Venues
        $venues_request      = new GuzzleHttp\Client();
        $venues_response     = $venues_request->get('http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=17&s=1&c='.$country_code.'&id='.$league_id);
        $getStatusCode       = $venues_response->getStatusCode();

        if ( $getStatusCode == "200" ) {
            $venues_data   = [];
            $data          =  $venues_response->getBody()->getContents();
            $venues_xml    = simplexml_load_string($data);
            $venue_info    = $venues_xml->List->Item;
            $venue         = isset($venue_info) ? $venue_info : [];
            foreach( $venue as $result )
            {

                foreach( (array) $result as $index => $val)
                {
                        $venues_data[] = $val;
                }

            }

            if ( !empty( $venues_data  ) && count($venues_data) > 0 )
            {
                return $venues_data;
            }
        }
    }

    /**
     * @param $data_info
     * @return array
     * @deprecated deprecated since self::formatToArray
     */
    public static function toarray($data_info)
    {
        $data = [];
        foreach( (array) $data_info as $index => $val)
        {
            $data[$index] = $val;
        }
        return $data;
    }

    public static function formatToArray($array)
    {
        return json_decode(json_encode((array) $array), TRUE);
    }
}
