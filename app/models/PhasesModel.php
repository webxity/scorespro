<?php
class PhasesModel extends \Eloquent
{
    protected $table = 'phases';
    
    protected $fillable = array('phase_id', 'competition_id', 'phase_code','phase_name');


    public static function insert_phases($data)
    {
        $phases_array = static::getAllPhases();
        if(!in_array($data['id'], $phases_array)){
            DB::table('phases')->insert(array(
                'phase_id'         => $data['id'], 
                'competition_id'   => $data['competition_id'],
                'phase_code'       => $data['code'],
                'phase_name'       => $data['name']
            ));
        }
    }

    public static function getAllPhases()
    { 
        $phases_data = [];
        
        foreach (PhasesModel::all() as $phases)
        {
            $phases_data[] = $phases->phase_id;
        }
        return $phases_data;
    }   

}
