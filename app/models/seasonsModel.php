<?php


class seasonsModel extends \Eloquent
{
    protected $table = 'seasons';
    
    protected $fillable = array('season_id', 'sport_id', 'season_code','season_name');


    public static function insert_seasons($season_data)
    {
        $seasons_array = static::getAllSeasons();
        foreach ( $season_data as $data )
        {
            if(!in_array($data['id'], $seasons_array)){
                DB::table('seasons')->insert(array(
                    'season_id'         => $data['id'], 
                    'sport_id'          => 1,
                    'season_code'       => $data['code'],
                    'season_name'       => $data['name']
                ));
            }
        }

        //echo "data added successfully";
    }

    public static function getSeasonId($data)
    {
        foreach( $data as $getId )
        {   
            $id[] = $getId['id'];
        }
        return $id;
    }
    
    public static function getSeasonById($season_name = '')
    {
        return DB::table('seasons')->where('season_name', $season_name)->pluck('season_id');
    }

    public static function getAllSeasons()
    {
        $seasons_data = [];

        foreach (seasonsModel::all() as $seasons)
        {

            $seasons_data[] = $seasons->season_id;
        }
        return $seasons_data;
    }

}