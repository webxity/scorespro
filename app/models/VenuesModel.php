<?php
class VenuesModel extends \Eloquent
{
    protected $table = 'venues';

    protected $fillable = array('venue_id', 'venue_name', 'venue_city','venue_address','venue_country','venue_zip','venue_capacity','venue_established','venue_url','venue_longitude','venue_latitude','venue_map_url','venue_surface','venue_facts','venue_description','venue_architect','venue_prev_name','venue_phone','venue_fax','venue_email');

    public static function insertVenuesInfo($venues_data)
    {
        $venues_id = static::getAllVenues();
        foreach ( $venues_data as $data )
        {
            if(!in_array($data['id'], $venues_id)){
                DB::table('venues')->insert(array(
                    'venue_id'          => $data['id'],
                    'venue_name'        => $data['name'],
                    'venue_city'        => $data['city'],
                    'venue_address'     => $data['address'],
                    'venue_country'     => $data['country'],
                    'venue_capacity'    => $data['capacity'],
                    'venue_established' => $data['established'],
                    'venue_zip'         => $data['zip'],
                    'venue_url'         => $data['url'],
                    'venue_longitude'   => $data['longitude'],
                    'venue_latitude'    => $data['latitude'],
                    'venue_map_url'     => $data['map_url'],
                    'venue_surface'     => $data['surface'],
                    'venue_facts'       => $data['facts'],
                    'venue_description' => $data['description'],
                    'venue_prev_name'   => $data['prev_name'],
                    'venue_phone'       => $data['phone'],
                    'venue_fax'         => $data['fax'],
                    'venue_email'       => $data['email'],
                    'venue_architect'   => $data['architect']
                ));
            }
        }
    }

    public static function getAllVenues()
    {
        $venues_data = [];
//        print_r(VenuesModel::all());exit();

        foreach (VenuesModel::all() as $venues)
        {

            $venues_data[] = $venues->venue_id;
        }
        return $venues_data;
    }

}