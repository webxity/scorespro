<?php
class MatchPeriodModel extends \Eloquent
{
    protected $table = 'match_period';

    protected $fillable = [
        'match_id',
        'period_id',
        'period_name',
        'detail_name',
        'detail_value'
    ];

    /**
     * @param $match_period_data
     * @return mixed
     */
    public static function insertPeroidResults($match_period_data)
    {
    	$match_period_detail_data = $match_period_data[0];
        return DB::table('match_period')->insert([
            'match_id' => MatchModel::$matchID,
            'period_id' => $match_period_data['@attributes']['id'],
            'period_name' => $match_period_data['@attributes']['name'],
            'detail_name' => $match_period_detail_data['@attributes']['name'],
            'detail_value' => $match_period_detail_data['@attributes']['value']
        ]);
    }
}