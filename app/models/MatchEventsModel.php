<?php
class MatchEventsModel extends \Eloquent
{
    protected $table = 'match_events';

    protected $fillable = array(
        'match_id',
        'match_hid_id',
        'event_type',
        'event_value',
    );

    private static $_errorAlias = 'MATCH_EVENT: ';

    private static $_internalData = null;

    private static $_events = [
        // Change in cards
        ['position' => 1, 'type' => 'cards'],

        // Change in correction time
        ['position' => 2, 'type' => 'ctTime'],

        // Change in date
        ['position' => 3, 'type' => 'date'],

        // Change in kickoff time
        ['position' => 4, 'type' => 'kickoffTime'],

        // Visible will be 1 if followed live, 0 if not live
        ['position' => 5, 'type' => 'visibility'],

        // League sort order of countries league, lower the number
        // higher the league importance
        ['position' => 6, 'type' => 'sortOrder'],

        // Change in notes description
        ['position' => 7, 'type' => 'note'],

        // Change in results, Mostly scores
        ['position' => 8, 'type' => 'results'],

        // Change in status half time, full time, extra time, penalty, finished
        ['position' => 9, 'type' => 'status'],

        // Substitution of home team
        ['position' => 10, 'type' => 'subHome'],

        // Substitution of away team
        ['position' => 11, 'type' => 'subAway']
    ];

    /**
     * @param array $data Data from API Call
     * @return $this
     */
    public static function setData($data) {
        self::$_internalData = $data;
    }

    /**
     * @param int $matchID
     * @param int|string $bitArray
     * @param int|string $matchHID
     * @return this|null
     */
    public function listen($matchID, $bitArray, $matchHID) {
        // Force bitArray to string conversion for comparison
        $bitArray = (string) $bitArray;

        if (!$matchID || !$bitArray) return $this->_log('match_id or bitArray is not provided');

        $match = MatchModel::getMatchInfo($matchID);

        MatchModel::updateMatchHid($matchID, $matchHID);

        /**
         * No event to process
         */
        if ($match && $match->bit_array === $bitArray) {
            return null;
        }

        $bitArrays = str_split($bitArray);

        $bitPositions = [];
        foreach ($bitArrays as $index => $value) {
            if ($value === '1') {
                $bitPositions[] = $index+1;
            }
        }

        return $this->_triggerEvent($bitPositions);
    }

    /**
     * Trigger event for bit positions
     * @param array $bitPositions
     * @return $this|null
     */
    private function _triggerEvent($bitPositions) {
        if (count($bitPositions) < 1) return null;

        foreach ($bitPositions as $k => $v) {
            foreach (self::$_events as $key => $val) {
                if ($val['position'] === $v) {
                    $event_type = $val['type'];
                    unset($bitPositions[$k]);
                    $this->_insert($event_type);
                    break;
                }
            }
        }

        return $this;
    }

    private function _insert($type) {
        $data = self::$_internalData;
        $match_id = $data['@attributes']['id'];

        $iData = [
            'match_id' => $match_id,
            'event_type' => $type
        ];

        $iData = array_merge($iData, MatchModel::timestamps());

        $this->_modifyBaseOnType($type, $iData, $data);

        DB::table('match_events')->insert($iData);
    }

    private function _modifyBaseOnType($type, &$iData , $data) {
        if ($type === "cards" || $type === 'results' || $type === 'subHome'
            || $type === 'subAway'
        ) {
            return false;
        }

        $attr = $data['@attributes'];

        if ($type === 'status') {
            $status = $attr['statustype'];
            if ($status === 'fin') {
                LeagueTableModel::request(null, $attr['leagueCode']);
            }

            return $iData['event_value'] = $attr['status'];
        }

        if ($type === 'ctTime') {
            return $iData['event_value'] = $attr['ct'];
        }

        if ($type === 'date') {
            return $iData['event_value'] = $attr['match_date'];
        }

        if ($type === 'kickoffTime') {
            return $iData['event_value'] = $attr['startTime'];
        }

        if ($type === 'sortOrder') {
            return $iData['event_value'] = $attr['leagueSort'];
        }

        if ($type === 'note') {
            $info = $data['Information'];
            if ($info && isset($info['note'])) {
                $note = (is_array($info['note']))
                    ? implode(',', $info['note'])
                    : $info['note'];

                return $iData['event_value'] = $note;
            }
        }
    }

    private function _log($msg = '') {
        return Log::warn($this->_errorAlias . $msg);
    }
}
