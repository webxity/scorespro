<?php
class ClientStructure
{

    public static function GetData()
    {
        static::getHistoryId();
        static::insertSeasons();
        static::insertCountries();
        static::insertCompetitions();
        static::insertLeagueTables();
        static::insertPhases();
        static::insertRounds();
        $competitionData = CompetitionsModel::getAllCompetitionsData();
        foreach ($competitionData as $competition ) {
             $country_code = countriesModel::getCountryCode($competition['country_id']);
             static::insertVenues($country_code);
             sleep(10);
        }
        foreach ($competitionData as $competition ) {
            static::insertTeam($competition['country_id'], $competition['competition_id']);
            sleep(10);
        }
        static::insertSquads();
    }

    public static function getHistoryId(){
        //Sports
        $sports_request      = new GuzzleHttp\Client();
        $sports_response     = $sports_request->get('http://data2.scorespro.com/exporter/?state=clientStructure&type=11');
        $getStatusCode       = $sports_response->getStatusCode();

        if ( $getStatusCode == "200" ) {
            $sports_data   = [];
            $data          =  $sports_response->getBody()->getContents();
            $sports_xml    = simplexml_load_string($data);
            $sport         = $sports_xml->List->Item;

            foreach( $sport as $result ) {
                foreach( (array) $result as $index => $val) {
                    if ( $val['name'] == 'SOCCER') {
                        $sports_data[] = $val;
                    }
                }
            }
           
            if ( !empty( $sports_data  ) && count($sports_data) > 0 )
            {
                if(SportsModel::getSportHid()) {
                    SportsModel::updateSportHid($sports_data[0]['hid'], $sports_data[0]['lineup_hid']);
                } else {
                    SportsModel::sports($sports_data);                    
                }
                return $sports_data;
            }
        }
    }

    public static function insertSeasons()
    {
        //Seasons
        $season_request         = new GuzzleHttp\Client();
        $season_response        = $season_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=12&s=1");
        $season_getStatusCode   = $season_response->getStatusCode();

        if ( $season_getStatusCode == "200" ) {
            $get_season_data     = $season_response->getBody()->getContents();
            $season_data_xml     = simplexml_load_string($get_season_data);
            $seasons             = $season_data_xml->List->Item;
            $season_data         = [];

            foreach ( $seasons as $season ) {
                foreach( (array) $season as $index => $val) {
                $season_data[] = $val;
                }
            }

            if ( !empty( $season_data  ) && count($season_data) > 0 ) {
                seasonsModel::insert_seasons($season_data);
            }
        }
    }

    public static function insertCountries()
    {
        //Countries
        $allSeasonsIds = seasonsModel::getAllSeasons();
        $countries_data_ids        =[];
        foreach ( $allSeasonsIds as $allSeasonsId ) {
            $countries_request         = new GuzzleHttp\Client();
            $countries_response        = $countries_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=13&ss='".$allSeasonsId."'");
            $get_countries_data        = $countries_response->getStatusCode();

            if ( $get_countries_data == "200" ) {
                $get_countries_data     = $countries_response->getBody()->getContents();
                $countries_data_xml     = simplexml_load_string($get_countries_data);
                $countries              = $countries_data_xml->List->Item;
                $countries_data         = [];
                foreach ( $countries as $country ) {
                    foreach( (array) $country as $index => $val) {
                       $countries_data_ids[] = $countries_data [] = $val;
                    }
                }
                if ( !empty( $countries_data  ) && count($countries_data) > 0 ) {
                    countriesModel::countries($countries_data, $allSeasonsId);
                }
            }
        }
    }

    public static function insertCompetitions()
    {
        $allCountriesIds = countriesModel::getAllCountries();
        //Competition
        $competitions_carry="";
        foreach ( $allCountriesIds as $allCountriesId )
        {

            $competition_request         = new GuzzleHttp\Client();
            $competition_response        = $competition_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=14&c=".$allCountriesId);
            $get_competition_data        = $competition_response->getStatusCode();
            if ( $get_competition_data == "200")
            {
                $get_competition_data     = $competition_response->getBody()->getContents();
                $competition_data_xml     = simplexml_load_string($get_competition_data);
                $competitions              = $competition_data_xml->List->Item;
                $competition_data         = [];
                if(isset($competitions) && !empty( $competitions  ))
                {
                    foreach ( $competitions as $competition )
                    {
                        foreach( (array) $competition as $index => $val)
                        {
                            $competition_des   = explode("AND", $val['description']);

                            $competitions_carry = trim($competition_des[1]);
                            $val['country_id'] = $allCountriesId;
                            $val['competition_parent_id'] = null;
                            if($competitions_carry=="IS LEAF"){
                                $competition_data = $val;
				                CompetitionsModel::competitions($competition_data);
                            }
                            else{
                                $competition_data = static::getCompetition($val,$allCountriesId);
                            }
                        }
                    }
                    if ( !empty( $competition_data  ) && count($competition_data) > 0 )
                    {
                        CompetitionsModel::competitions($competition_data);
                    }
                }
            }
        }
    }

    public static function insertLeagueTables()
    {
        LeagueTableModel::GetData();
    }

    public static function insertSquads()
    {
        $competitionIds = CompetitionsModel::getAllCompetitionsId();

        foreach ( $competitionIds as $competitionID ) {
            $competition_request = new GuzzleHttp\Client();
            $url = "http://data2.scorespro.com/exporter/?state=clientStructure&type=27&l={$competitionID}";

            $competition_response = $competition_request->get($url);
            $get_competition_data = $competition_response->getStatusCode();
            if ( $get_competition_data == "200")
            {
                $get_competition_data = $competition_response->getBody()->getContents();
                $competition_data_xml = simplexml_load_string($get_competition_data);
                $teams = ClientUpdate::formatToArray($competition_data_xml->List);

                if (isset($teams['Team']) && !empty($teams['Team'])) {
                    $teams = $teams['Team'];

                    if (isset($teams['@attributes'])) {
                        SquadsModel::insert($teams, $competitionID);
                        continue;
                    }

                    SquadsModel::insertCollection($teams, $competitionID);
                }
            }
        }
    }

    public static function insertPhases()
    {
        //Phases
        $allCompetitionsIds     = CompetitionsModel::getAllCompetitions();
        foreach ($allCompetitionsIds as $allCompetitionsId) {
            # code...
            $phases_request         = new GuzzleHttp\Client();
            $phases_response        = $phases_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=15&c=".$allCompetitionsId['country_id']."&id=".$allCompetitionsId['competition_id']);
            $phases_getStatusCode   = $phases_response->getStatusCode();

            if ( $phases_getStatusCode == "200" )
            {
                $get_phases_data     = $phases_response->getBody()->getContents();
                $phases_data_xml     = simplexml_load_string($get_phases_data);
                $phases_raw_data     = $phases_data_xml->List->Item;
                $phases_data         = [];

                foreach ( $phases_raw_data as $phases )
                {
                    foreach( (array) $phases as $index => $val)
                    {
                        $phases_des = explode("AND", $val['description']);
                        $phases_carry = trim($phases_des[1]);
                        if($phases_carry=="IS LEAF"){
                            $val['competition_id'] = $allCompetitionsId['competition_id'];
                            $phases_data = $val;
                        }else{
                            $phases_data = static::getPhases($val,$allCompetitionsId['competition_id'],$allCompetitionsId['country_id']);
                        }
                    }
                }

                if ( !empty( $phases_data  ) && count($phases_data) > 0 )
                {
                    PhasesModel::insert_phases($phases_data);
                }
            }
        }
    }

    public static function insertRounds()
    {
        $allCompetitionsIds     = CompetitionsModel::getAllCompetitions();
        //Round
        foreach ($allCompetitionsIds as $allCompetitionsId) {
            $round_request         = new GuzzleHttp\Client();
            $round_response        = $round_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=16&c=".$allCompetitionsId['country_id']."&id=".$allCompetitionsId['competition_id']);
            $round_getStatusCode   = $round_response->getStatusCode();

            if ( $round_getStatusCode == "200" )
            {
                $get_round_data     = $round_response->getBody()->getContents();
                $round_data_xml     = simplexml_load_string($get_round_data);
                $rounds             = $round_data_xml->List->Item;
                $round_data         = [];

                foreach ( $rounds as $round )
                {
                    foreach( (array) $round as $index => $val)
                    {
                        $val['competition_id']=$allCompetitionsId['competition_id'];
                        $round_data[] = $val;
                    }
                }

                if ( !empty( $round_data  ) && count($round_data) > 0 )
                {
                    RoundsModel::insert_rounds($round_data);
                }
            }
        }
    }

    public static function getCompetition($data = [] , $CountryId = 0)
    {
        # code...
        $competition_request         = new GuzzleHttp\Client();
        $competition_response        = $competition_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=14&c=".$data['id']);
        $get_competition_data        = $competition_response->getStatusCode();
        if ( $get_competition_data == "200")
        {
            $get_competition_data     = $competition_response->getBody()->getContents();
            $competition_data_xml     = simplexml_load_string($get_competition_data);
            $competitions              = $competition_data_xml->List->Item;
            $competition_data         = [];
            if(isset($competitions) && !empty( $competitions  ))
            {
                foreach ( $competitions as $competition )
                {
                    foreach( (array) $competition as $index => $val)
                    {
                        $competition_des   = explode("AND", $val['description']);

                        $competitions_carry = trim($competition_des[1]);
                        $val['country_id'] = $CountryId;
                        $val['competition_parent_id'] = $data['id'];
                        if($competitions_carry=="IS LEAF"){
                            $competition_data = $val;
				CompetitionsModel::competitions($competition_data);
                        }else{
                            $competition_data = static::getCompetition($val,$CountryId);
                        }
                    }
                }
                if ( !empty( $competition_data  ) && count($competition_data) > 0 )
                {
                    CompetitionsModel::competitions($competition_data);
                }
            }
        }
    }

    public static function getPhases($data = [] , $CompetitionId = 0, $CountryId = 0)
    {
        # code...
        $phases_request         = new GuzzleHttp\Client();
        $phases_response        = $phases_request->get("http://data2.scorespro.com/exporter/?state=clientStructure&type=15&c=".$CountryId."&id=".$data['id']);
        $get_phases_data        = $phases_response->getStatusCode();
        if ( $get_phases_data == "200")
        {
            $get_phases_data     = $phases_response->getBody()->getContents();
            $phases_data_xml     = simplexml_load_string($get_phases_data);
            $phases              = $phases_data_xml->List->Item;
            $phases_data         = [];
            if(isset($phases) && !empty( $phases  ))
            {
                foreach ( $phases as $phase )
                {
                    foreach( (array) $phase as $index => $val)
                    {
                        $phases_des   = explode("AND", $val['description']);

                        $phases_carry = trim($phases_des[1]);
                        if($phases_carry=="IS LEAF"){

                            $val['competition_id'] = $CompetitionId;
                            $phases_data = $val;

                        }else{
                            $phases_data = static::getPhases($val,$CompetitionId,$CountryId);
                        }
                    }
                }
                if ( !empty( $phases_data  ) && count($phases_data) > 0 )
                {
                    PhasesModel::insert_phases($phases_data);
                }
            }
        }
    }

    public static function insertVenues($country_code = ''){
        //Venues
        $venues_request      = new GuzzleHttp\Client();
        $venues_response     = $venues_request->get('http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=28&s=1&code='.$country_code);
        $getStatusCode       = $venues_response->getStatusCode();
        if ( $getStatusCode == "200" ) {
            $venues_data   = [];
            $data          =  $venues_response->getBody()->getContents();
            $venues_xml    = simplexml_load_string($data);
            $venue_info    = (array) $venues_xml->List;
            $venue         = isset($venue_info['Venue']) ? $venue_info['Venue'] : [];
            foreach( $venue as $result )
            {

                foreach( (array) $result as $index => $val)
                {
                        $venues_data[] = $val;
                }

            }

            if ( !empty( $venues_data  ) && count($venues_data) > 0 )
            {
                VenuesModel::insertVenuesInfo($venues_data);
            }
        }
    }

    public static function insertTeam($country_code = '', $league_id = 0){
        //Team
        $team_request      = new GuzzleHttp\Client();
        $team_response     = $team_request->get('http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&type=17&s=1&c='.$country_code.'&id='.$league_id);
        $getStatusCode       = $team_response->getStatusCode();

        if ( $getStatusCode == "200" ) {
            $team_data   = [];
            $data        =  $team_response->getBody()->getContents();
            $team_xml    = simplexml_load_string($data);
            $tema_info   = $team_xml->List->Item;
            $team       = isset($tema_info) ? $tema_info : [];
            foreach( $team as $result )
            {

                foreach( (array) $result as $index => $val)
                {
                        $team_data[] = $val;
                }

            }

            if ( !empty( $team_data  ) && count($team_data) > 0 )
            {
                TeamsModel::insertTeamsInfo($team_data, $country_code);
            }
        }
    }
}
