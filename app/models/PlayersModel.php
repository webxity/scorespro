<?php


class PlayersModel extends \Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'players';

    protected $fillable = [
        'player_id',
        'name',
        'nationality',
        'shirt',
        'position',
        'active'
    ];

    /**
     * @param {array} $player
     * @param callback $func
     * @return mixed
     */
    public static function insert($player, callable $func = null)
    {
        $player = $player['@attributes'];
        $player_id = $player['id'];

        $data = [
            'name' => $player['name'],
            'nationality' => $player['nationality'],
            'shirt' => $player['shirt'],
            'position' => $player['position'],
            'active' => $player['active']
        ];

        if (self::getPlayerById($player_id)) {
            Log::info("Player({$player_id}) already exist. Trying to update...");

            $data['player_id'] = $player_id;
            if (!self::getPlayerByData($data)) {
                return DB::table('players')
                    ->where('player_id', $player_id)
                    ->update($data);
            }
        } else {
            $data['player_id'] = $player_id;
            DB::table('players')->insert($data);

            call_user_func($func, $player_id);
        }
    }

    public static function getPlayerById($id)
    {
        return self::where('player_id', '=', $id)->first();
    }

    public static function getPlayerByData($where)
    {
        return self::where($where)->first();
    }
}