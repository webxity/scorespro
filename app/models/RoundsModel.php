<?php


class RoundsModel extends \Eloquent
{
    protected $table = 'rounds';
    

    protected $fillable = array('round_id', 'competition_id', 'round_name');

    public static function insert_rounds($round_data)
    {
        $rounds_array = static::getAllRounds();
        foreach ( $round_data as $data )
        {
            if(!in_array($data['id'], $rounds_array)){
                DB::table('rounds')->insert(array(
                    'round_id'         => $data['id'], 
                    'competition_id'   => $data['competition_id'],
                    'round_name'       => $data['name']
                ));
            }
        }

        //echo "data added successfully";
    }

    public static function getAllRounds()
    { 
        $rounds_data = [];
        
        foreach (RoundsModel::all() as $rounds)
        {
            $rounds_data[] = $rounds->round_id;
        }
        return $rounds_data;
    }

}