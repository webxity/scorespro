<?php


class SportsModel extends \Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sports';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   // protected $hidden = array('password', 'remember_token');

    protected $fillable = array('sport_id', 'sport_code', 'sport_name','sport_hid','sport_lineup_hid');

    public static $hid = '';
    public static $lineupHid = '';

    public static function sports($sports_data)
    {
        foreach ( $sports_data as $data ) {
            DB::table('sports')->insert(array(
                'sport_id'          => 1,
                'sport_code'        => $data['code'],
                'sport_name'        => $data['name'],
                'sport_hid'         => $data['hid'],
                'sport_lineup_hid'  => $data['lineup_hid'],
            ));
        }

        //echo "data added successfully";
    }

    public static function updateSportHid($hid, $lineup_id = null)
    {
        $update = [];
        $update['sport_hid'] = $hid;

        if ($lineup_id) {
            $update['sport_lineup_hid'] = $lineup_id;
        }

        DB::table('sports')
            ->where('sport_id', 1)
            ->update($update);

        self::$hid = $hid;
        self::$lineupHid = $lineup_id;
    }

    public static function getSportHid()
    {
        $hid = SportsModel::first();
        return isset($hid) ? $hid->sport_hid : '';
    }
}
