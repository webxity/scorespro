<?php

class MatchSubsModel extends \Eloquent
{
    protected $table = 'match_substitution';

    protected $fillable = [
        'match_id',
        'team_id',
        'player_in_id',
        'player_out_id',
        'player_in_name',
        'player_out_name',
        'sub_minute',
        'sub_minute2'
    ];

    public static function insertSubstitution($substitution_data)
    {
        $subs = $substitution_data['Sub'];
        if (count($subs) > 1) {
            foreach($subs as $sub) {
                self::_insert($sub);
            }
        } else {
            self::_insert($subs);
        }

        MatchModel::$teamID = null;
    }

    private static function _insert($data)
    {
        $sub = $data['@attributes'];

        $d = [
            'match_id' => MatchModel::$matchID,
            'team_id' => MatchModel::$teamID,
            'player_in_id' => $sub['pin_id'],
            'player_out_id' => $sub['pout_id'],
            'player_in_name' => $sub['pin_name'],
            'player_out_name' => $sub['pout_name'],
            'sub_minute' => $sub['min'],
            'sub_minute2' => $sub['min2'],
        ];

        if (!self::_getSubstitute($d)) {
            DB::table('match_substitution')->insert($d);
        }
    }

    private static function _getSubstitute($data) {
        return self::where($data)->first();
    }
}