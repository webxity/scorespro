<?php


class countriesModel extends \Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   // protected $hidden = array('password', 'remember_token');




   protected $fillable = array('country_id', 'season_id', 'country_code','country_name');


    public static function countries($countries_data , $allSeasonsIds)
    { 
        $country_array = static::getAllCountries();
        foreach ( $countries_data as $data )
        {
            if(!in_array($data['id'], $country_array)){
                DB::table('countries')->insert(array(
                    'country_id'         => $data['id'], 
                    'season_id'          => $allSeasonsIds,
                    'country_code'       => $data['code'],
                    'country_name'       => $data['name']
                ));
            }
        }
    }

    public static function getAllCountries()
    { 
        $country_data = [];
        
        foreach (countriesModel::all() as $country)
        {
            $country_data[] = $country->country_id;
        }
        return $country_data;
    }

    public static function getCountryId($name = '', $season_id = '')
    {
        return DB::table('countries')
            ->where('country_name' , '=' ,  $name)
            ->where('season_id', '=' , $season_id)
            ->get(['country_code','country_id']);
    }
    public static function getCountryCode($id = '')
    {
        return DB::table('countries')
            ->where('country_id' , '=' ,  $id)
            ->pluck('country_code');
    }
}
