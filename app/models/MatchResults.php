<?php
class MatchResultsModel extends \Eloquent
{
    protected $table = 'match_results';

    protected $fillable = [
        'match_id',
        'result_id',
        'result_name',
        'result_value'
    ];

    public static function insertMatchResults($match_results_data)
    {
        foreach ( $match_results_data as $match_results ) {
            DB::table('match_results')->insert([
                'match_id' => MatchModel::$matchID,
                'result_id' => $match_results['id'],
                'result_name' => $match_results['name'],
                'result_value' => $match_results['value']
            ]);
        }
    }

}