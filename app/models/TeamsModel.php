<?php
class TeamsModel extends \Eloquent
{
    protected $table = 'teams';

    protected $fillable = array('team_id', 'country_id', 'venue_id','team_name','team_logo');

    public static function insertTeamsInfo($teams_data, $country_id)
    {
        $teams_id = static::getAllTeams();
        foreach ( $teams_data as $data )
        {
            if(!in_array($data['id'], $teams_id)){
                DB::table('teams')->insert(array(
                    'team_id'       => $data['id'],
                    'country_id'    => $country_id,
                    'venue_id'      => $data['venue_id'],
                    'team_name'     => $data['name'],
                    'team_logo'     => $data['logo']
                ));
            }
        }
    }

    public static function getAllTeams()
    {
        $teams_data = [];

        foreach (teamsModel::all() as $teams) {
            $teams_data[] = $teams->team_id;
        }

        return $teams_data;
    }

    public static function getTeamById($id = 0)
    {
        return self::where('team_id', $id)->first();
    }


}
