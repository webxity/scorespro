<?php
class MatchModel extends \Eloquent
{
    protected $table = 'match';

    protected $fillable = [
        'match_id',
        'competition_id',
        'ct',
        'match_date',
        'last_period',
        'league_type',
        'start_time',
        'match_status',
        'status_type',
        'match_type',
        'visible',
        'lineups',
        'home_team_id',
        'away_team_id',
        'hid'
    ];

    public static $matchID = null;
    public static $teamID = null;
    public static $awayTeamID = null;
    public static $homeTeamID = null;

    public static function insertMatch($match_data)
    {
        $is_match = self::getMatch($match_data['id']);
        $is_competition = CompetitionsModel::getCompetitionById($match_data['leagueCode']);

        /**
         * If competition is not found, fail silently
         */
        if (!$is_competition) {
            $msg = "Competition({$match_data['leagueCode']}) not found for Match({$match_data['id']})";
            return Log::critical($msg);
        }

        $iData = [
            'competition_id' => $match_data['leagueCode'],
            'ct' => $match_data['ct'],
            'match_date' => $match_data['match_date'],
            'last_period' => $match_data['lastPeriod'],
            'league_type' => $match_data['leagueType'],
            'start_time' => $match_data['startTime'],
            'match_status' => $match_data['status'],
            'status_type' => $match_data['statustype'],
            'match_type' => $match_data['type'],
            'visible' => $match_data['visible'],
            'lineups' => $match_data['lineups'],
            'home_team_id' => $match_data['home_team_id'],
            'away_team_id' => $match_data['away_team_id']
        ];

        if (isset($match_data['hid'])) {
            $iData['hid'] = $match_data['hid'];
        }

        if (isset($match_data['venue']) && !empty($match_data['venue'])) {
            $iData['venue'] = $match_data['venue'];
        }

        if (isset($match_data['referee']) && !empty($match_data['referee'])) {
            $iData['referee'] = $match_data['referee'];
        }

        $iData = array_merge($iData, self::timestamps());

        if ($is_match) {
            unset($iData['created_at']);
            return DB::table('match')
                ->where('match_id', '=', $match_data['id'])
                ->update($iData);
        } else {
            $iData['match_id'] = $match_data['id'];
            return DB::table('match')->insert($iData);
        }
    }

    public static function getAllMatch()
    {
        $match_data = [];
        foreach (MatchModel::all() as $match) {
            $match_data[] = $match->match_id;
        }
        return $match_data;
    }

    /**
     * Get match by id
     * @param $id
     */
    public static function getMatch($id) {
        return DB::table('match')
            ->where('match_id', '=', $id)->first();
    }

    /**
     * Get match info by id
     * @param $id
     * @return Eloquent
     */
    public static function getMatchInfo($id) {
        return DB::table('match')
            ->join('match_info', 'match_info.match_id', '=', 'match.match_id')
            ->where('match.match_id', $id)->first();
    }

    public static function getMatchHid()
    {
        $hid = DB::table('match')
            ->orderBy('hid', 'desc')
            ->where('id', '<>', '')
            ->pluck('hid');

        // Fallback to history id from sports table
        if (!$hid) {
            $hid = DB::table('sports')
                ->where('sport_id', '=', 1)
                ->pluck('sport_hid');

            if ($hid) {
                $hid -= 1;
            }
        }

        return $hid;
    }

    /**
     * Update match
     * @param $matchID
     * @param $matchHID
     */
    public static function updateMatchHid($matchID, $matchHID)
    {
        return DB::table('match')
            ->where('match_id', $matchID)
            ->update([
                'hid' => $matchHID
            ]);
    }

    /**
     * Update match
     * @param int $matchID
     * @param array $data
     * @return mixed
     */
    public static function updateMatchById($matchID, $data)
    {
        if (!is_array($data)) {
            return null;
        }
        return DB::table('match')
            ->where('match_id', $matchID)
            ->update($data);
    }

    /**
     * Update match_hid
     * @param $oldMatchHID
     * @param $matchHID
     */
    public static function updateMatchHidByHid($oldMatchHID,$matchHID)
    {
        return DB::table('match')
            ->where('hid', $oldMatchHID)
            ->update([
                'hid' => $matchHID
            ]);
    }

    /**
     * @return array
     */
    public static function timestamps()
    {
        return [
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
    }
}
