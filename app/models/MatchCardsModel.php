<?php
class MatchCardsModel extends \Eloquent
{
    protected $table = 'match_cards';

    protected $fillable = [
        'match_id',
        'team_id',
        'card_type',
        'player_name',
        'match_time',
        'p_id'
    ];

    public static function insertMatchCard($match_card_data)
    {
        $cards = $match_card_data['Card'];
        if (count($cards) > 1) {
            foreach($cards as $card) {
                self::_insert($card);
            }
        } else {
            self::_insert($cards);
        }

        MatchModel::$teamID = null;
    }

    private static function _insert($data)
    {
        $card = $data['@attributes'];

        $d = [
            'match_id' => MatchModel::$matchID,
            'team_id' => MatchModel::$teamID,
            'card_type' => $card['type'],
            'player_name' => $card['name'],
            'match_time' => $card['time'],
            'p_id' => $card['p_id']
        ];

        if (!self::_getCards($d)) {
            DB::table('match_cards')->insert($d);
        }
    }

    private static function _getCards($data) {
        return self::where($data)->first();
    }

}