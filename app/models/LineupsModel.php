<?php
class LineupsModel extends \Eloquent
{
    protected $table = 'lineups';

    protected $fillable = [
        'team_id',
        'match_id',
        'name',
        'standing',
        'coach',
        'formation',
        'lineup_hid'
    ];
    
    public static function GetData()
    {
        $hid = self::getLineUpHid();
        $lineups_request = new GuzzleHttp\Client();
        #$url = "http://data2.scorespro.com/docs/xml-samples/soccer_lineups.xml";
        $url = "http://data2.scorespro.com/exporter/?state=clientUpdate&usr=14weklapsa&pwd=GiJ89tTs&s=1&type=26&h={$hid}";
        $lineups_response = $lineups_request->get($url);
        $getStatusCode = $lineups_response->getStatusCode();
        
        if ( $getStatusCode == "200" ) {
            $data =  $lineups_response->getBody()->getContents();
            $sports_xml = simplexml_load_string($data);
            $match_hid_data = ClientUpdate::formatToArray($sports_xml->Sport);

            $err = $sports_xml->GeneralInformation->Error;
            if ($err) {
                return Log::critical($err);
            }

            $match_lineup_hid = $match_hid_data['@attributes']['lineup_hid'];

            /**
             * No match linesups found, skip it
             */
            if (!isset($match_hid_data['Matchday'])) {
                return null;
            }

            $MatchDays = $match_hid_data['Matchday'];

            if (isset($MatchDays['@attributes'])) {
                $MatchDays = [$MatchDays];
            }

            foreach ($MatchDays as $MatchDay) {
                $match_date = $MatchDay['@attributes']['date'];
                $matches = isset($MatchDay['Match'])
                    ? $MatchDay['Match']
                    : null;

                /**
                 * No match found, skip
                 */
                if (!$matches) {
                    continue;
                }

                if ($matches && isset($matches['@attributes'])) {
                    $matches = [$matches];
                }

                foreach ($matches as $match) {
                    MatchModel::$matchID = $matchID = $match['@attributes']['id'];
                    $match['@attributes']['match_date'] = $match_date;

                    $home_team = $match['Home'];
                    $away_team = $match['Away'];

                    $home_team_id = $home_team['@attributes']['id'];
                    $away_team_id = $away_team['@attributes']['id'];

                    $match['@attributes']['home_team_id'] = $home_team_id;
                    $match['@attributes']['away_team_id'] = $away_team_id;

                    $match_id = $match['@attributes']['id'];

                    MatchModel::insertMatch($match['@attributes']);

                    $data = [
                        'lineup_hid' => $match_lineup_hid,
                        'match_id' => $match_id
                    ];

                    if ($home_team) {
                        self::insert($data, $home_team);
                    }

                    if ($away_team) {
                        self::insert($data, $away_team);
                    }

                    $data = null;

                    if (isset($home_team['Substitutes'])) {
                        MatchModel::$teamID = $home_team_id;
                        MatchSubsModel::insertSubstitution($home_team['Substitutes']);
                    }

                    if (isset($away_team['Substitutes'])) {
                        MatchModel::$teamID = $away_team_id;
                        MatchSubsModel::insertSubstitution($away_team['Substitutes']);
                    }
                }
            }
        }
    }
    
    public static function getLineUpHid()
    {
        $lineup_hid = DB::table('lineups')
            ->orderBy('lineup_hid', 'desc')
            ->where('id', '<>', '')
            ->pluck('lineup_hid');

        // Fallback to lineup history id from sports table
        if (!$lineup_hid) {
            $lineup_hid = DB::table('sports')
                ->where('sport_id', '=', 1)
                ->pluck('sport_lineup_hid');

            if ($lineup_hid) {
                $lineup_hid -= 1;
            }
        }

        return $lineup_hid;
    }

    /**
     * @param array $lineup_data
     * @param array $team
     * @return mixed
     */
    public static function insert($lineup_data, $team)
    {
        $_team = $team['@attributes'];

        $is_match = MatchModel::getMatch($lineup_data['match_id']);

        if (!$is_match) {
            $msg = "Match({$lineup_data['match_id']}) not found";
            return Log::warning($msg);
        }

        $lineup_data['team_id'] = $_team['id'];
        $lineup_data = array_merge($lineup_data, $_team);
        unset($lineup_data['id']);

        if (isset($team['Lineups'])) {
            MatchLineUpsModel::insert($lineup_data, $team['Lineups']);
        }

        return DB::table('lineups')->insert($lineup_data);
    }
}
