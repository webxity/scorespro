<?php

class MatchInfoModel extends \Eloquent
{
    protected $table = 'match_info';

    protected $fillable = [
        'match_id',
        'season_id',
        'country_id',
        'parent_competition_id',
        'root_competition_id',
        'short_name',
        'round',
        'note',
        'bit_array',
        'info_timestamp'
    ];

    public static function insertMatchInfo($data)
    {
        $competition_id = $data['root_competition_id']
            ? $data['root_competition_id']
            : $data['parent_competition_id'];

        if (MatchModel::getMatch($data['match_id']) && CompetitionsModel::getCompetitionById($competition_id)) {
            /**
             * Listening for bitArray change since last hid
             */
            $match_events = new MatchEventsModel;
            $match_events->listen($data['match_id'], $data['bitArray'], $data['hid']);

            if (MatchModel::getMatchInfo($data['match_id'])) {
                return self::_updateMatchInfo($data);
            }

            $note = (is_array($data['note']))
                ? implode(',', $data['note'])
                : $data['note'];

            DB::table('match_info')->insert([
                'match_id' => $data['match_id'],
                'season_id' => $data['season_id'],
                'country_id' => $data['country_id'],
                'parent_competition_id' => $data['parent_competition_id'],
                'root_competition_id' => $data['root_competition_id'],
                'short_name' => $data['shortName'],
                'round' => $data['round'],
                'note' => $note,
                'bit_array' => $data['bitArray'],
                'info_timestamp' => $data['timestamp']
            ]);
        }
    }

    /**
     * Update match info
     * @param array $data
     */
    private static function _updateMatchInfo($data)
    {
        return DB::table('match_info')
            ->where('match_id', $data['match_id'])
            ->update([
                'match_id' => $data['match_id'],
                'season_id' => $data['season_id'],
                'country_id' => $data['country_id'],
                'parent_competition_id' => $data['parent_competition_id'],
                'root_competition_id' => $data['root_competition_id'],
                'short_name' => $data['shortName'],
                'round' => $data['round'],
                'note' => $data['note'],
                'bit_array' => $data['bitArray'],
                'info_timestamp' => $data['timestamp']
            ]);
    }

    public static function getAllMatchInfo()
    {
        $match_data = [];

        foreach (MatchInfoModel::all() as $match) {
            $match_data[] = $match->match_id;
        }
        return $match_data;
    }
}