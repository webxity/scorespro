<?php

class MatchScorerModel extends \Eloquent
{
    protected $table = 'match_scorer';

    protected $fillable = [
        'match_id',
        'team_id',
        'player_name',
        'period_id',
        'match_time',
        'score_type',
        'p_id'
    ];

    public static function insertScorer($scorer_data)
    {
        $scorers = $scorer_data['Scorer'];
        if (count($scorers) > 1) {
            foreach($scorers as $scorer) {
                self::_insert($scorer);
            }
        } else {
            self::_insert($scorers);
        }

        MatchModel::$teamID = null;
    }

    private static function _insert($data)
    {
        $scorer = $data['@attributes'];

        $d = [
            'match_id' => MatchModel::$matchID,
            'team_id' => $scorer['team'],
            'player_name' => $scorer['name'],
            'period_id' => $scorer['period'],
            'match_time' => $scorer['time'],
            'score_type' => $scorer['type'],
            'p_id' => $scorer['p_id']
        ];

        if (!self::_getScorer($d)) {
            DB::table('match_scorer')->insert($d);
        }
    }

    private static function _getScorer($data) {
        return self::where($data)->first();
    }
}